import json
import sys

import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets
from PyQt5.QtWidgets import QMainWindow, QAction, QTableWidget, QTableWidgetItem, QPushButton
from PyQt5.QtWidgets import QMenu

fields = ['name', 'surname', 'company', 'phone', 'email', 'comment']
labels = ["Imię", "Nazwisko", "Firma", "Telefon", "Email", "Komentarz"]


class AddressBookTableWidget(QTableWidget):
    def __init__(self, people_list):
        self.row_count = len(people_list)
        QTableWidget.__init__(self, self.row_count, 6)
        self.setData(people_list)
        self.setMinimumSize(200, 300)
        for i in range(6):
            self.horizontalHeader().setSectionResizeMode(i, PyQt5.QtWidgets.QHeaderView.Stretch)

    def setData(self, people_list):
        for i, person in enumerate(people_list):
            self.setRowData(i, person)
        self.setHorizontalHeaderLabels(labels)

    def setRowData(self, row, person):
        for column, field in enumerate(fields):
            widget = QTableWidgetItem(person[field])
            self.setItem(row, column, widget)


class AddressBookMainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.setGeometry(300, 300, 600, 400)
        self.setWindowTitle('Książka adresowa')

        central_widget = PyQt5.QtWidgets.QWidget(self)
        label = PyQt5.QtWidgets.QLabel("Utwórz lub otwórz książkę adresową wybierając opcję w menu Plik")
        main_layout = PyQt5.QtWidgets.QVBoxLayout(central_widget)
        main_layout.addWidget(label, 0, PyQt5.QtCore.Qt.AlignCenter)
        self.setCentralWidget(central_widget)

        self.show()
        self.__create_menu()

    def __create_menu(self):
        new_action = QAction(self)
        new_action.setText("Nowy")
        new_action.triggered.connect(self.__create_address_book)

        open_action = QAction(self)
        open_action.setText("Otwórz")
        open_action.triggered.connect(self.__open_address_book)

        self.save_action = QAction(self)
        self.save_action.setText("Zapisz")
        self.save_action.setEnabled(False)
        self.save_action.triggered.connect(self.__save_address_book)

        file = QMenu("Plik", self)
        file.addAction(new_action)
        file.addAction(open_action)
        file.addAction(self.save_action)

        author_action = QAction(self)
        author_action.setText("Autor")
        author_action.triggered.connect(self.__about_author)

        about = QMenu("O programie", self)
        about.addAction(author_action)

        menu_bar = self.menuBar()
        menu_bar.addMenu(file)
        menu_bar.addMenu(about)

    def __save_address_book(self, button):
        files = PyQt5.QtWidgets.QFileDialog.getSaveFileName(caption="Wybierz nazwę pliku książki adresowej")
        if files[0]:
            try:
                with open(files[0], "w") as file:
                    file.write(json.dumps(self.__get_people_list()))

                PyQt5.QtWidgets.QMessageBox.information(
                    None,
                    "Książka adresowa",
                    "Poprawnie zapisano książkę adresową"
                )
            except (Exception):
                PyQt5.QtWidgets.QMessageBox.critical(
                    None,
                    "Błąd",
                    "Wystąpił błąd podczas zapisu do pliku. Upewnij się, że jest on dostępny i zawiera książkę adresową."
                )

    def __open_address_book(self, button):
        files = PyQt5.QtWidgets.QFileDialog.getOpenFileName(caption="Wybierz plik książki adresowej")
        if files[0]:
            try:
                w = ''.join(open(files[0], "r").readlines())
                people_list = json.loads(w)
                people_list.sort(key=lambda k: k['surname'])
                self.__create_table(people_list)
            except (Exception):
                PyQt5.QtWidgets.QMessageBox.critical(
                    None,
                    "Błąd",
                    "Wystąpił błąd podczas odczytu z pliku. Upewnij się, że jest on dostępny i zawiera książkę adresową."
                )

    def __about_author(self, button):
        PyQt5.QtWidgets.QMessageBox.information(
            None,
            "O programie",
            "Autor: Marcel Korpal\nNumer indeksu: 175911\nOpis programu: książka adresowa pozwalająca zapisywać podstawowe informacje o osobach."
        )

    def __create_address_book(self, button):
        self.__create_table([])

    def __create_table(self, people_list):
        self.save_action.setEnabled(True)

        add_button = QPushButton('Dodaj', self)
        add_button.clicked.connect(self.__on_add_button_clicked)
        delete_button = QPushButton('Usuń', self)
        delete_button.clicked.connect(self.__on_delete_button_clicked)

        self.__address_book_table_widget = AddressBookTableWidget(people_list)

        search_label = PyQt5.QtWidgets.QLabel()
        search_label.setText("Szukaj")
        search_entry = PyQt5.QtWidgets.QLineEdit()
        search_entry.textChanged.connect(self.__on_search_edited)

        central_widget = PyQt5.QtWidgets.QWidget(self)
        main_layout = PyQt5.QtWidgets.QGridLayout(central_widget)
        h_layout_top = PyQt5.QtWidgets.QHBoxLayout()
        h_layout_bottom = PyQt5.QtWidgets.QHBoxLayout()
        h_layout_top.addWidget(search_label, 1, PyQt5.QtCore.Qt.AlignRight)
        h_layout_top.addWidget(search_entry, 0, PyQt5.QtCore.Qt.AlignRight)
        h_layout_bottom.addWidget(add_button, 0, PyQt5.QtCore.Qt.AlignLeft)
        h_layout_bottom.addWidget(delete_button, 1, PyQt5.QtCore.Qt.AlignLeft)

        main_layout.addLayout(h_layout_top, 0, 0)
        main_layout.addWidget(self.__address_book_table_widget, 1, 0)
        main_layout.addLayout(h_layout_bottom, 2, 0)

        self.setCentralWidget(central_widget)

    def __on_search_edited(self, search_text):
        for row in range(self.__address_book_table_widget.row_count):
            row_text = ''
            for column in range(6):
                row_text += ' ' + self.__address_book_table_widget.item(row, column).text()
            if search_text.lower() in row_text.lower():
                self.__address_book_table_widget.showRow(row)
            else:
                self.__address_book_table_widget.hideRow(row)

    def __on_add_button_clicked(self, widget):
        self.__address_book_table_widget.setRowCount(self.__address_book_table_widget.row_count + 1)
        self.__address_book_table_widget.setRowData(self.__address_book_table_widget.row_count, {
            'name': '...',
            'surname': '',
            'company': '',
            'phone': '',
            'email': '',
            'comment': ''
        })
        self.__address_book_table_widget.row_count = self.__address_book_table_widget.row_count + 1

    def __on_delete_button_clicked(self, widget):
        row = self.__address_book_table_widget.currentRow()
        if row > -1:
            self.__address_book_table_widget.removeRow(row)
            self.__address_book_table_widget.row_count = self.__address_book_table_widget.row_count - 1

    def __get_people_list(self):
        list = []
        for row in range(self.__address_book_table_widget.row_count):
            item = {}
            for column in range(6):
                item[fields[column]] = self.__address_book_table_widget.item(row, column).text()
            list.append(item)
        return list


if __name__ == '__main__':
    application = PyQt5.QtWidgets.QApplication(sys.argv)
    translator = PyQt5.QtCore.QTranslator()
    translator.load("qt_" + PyQt5.QtCore.QLocale.system().name(),
                    PyQt5.QtCore.QLibraryInfo.location(PyQt5.QtCore.QLibraryInfo.TranslationsPath))
    application.installTranslator(translator)
    try:
        ex = AddressBookMainWindow()
        sys.exit(application.exec_())
    except (Exception):
        PyQt5.QtWidgets.QMessageBox.critical(
            None,
            "Błąd",
            "Wystąpił błąd podczas uruchomienia książki adresowej."
        )
