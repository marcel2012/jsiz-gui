import json

import gi

gi.require_version("Gtk", "3.0")

import gi.repository.Gtk

fields = ['name', 'surname', 'company', 'phone', 'email', 'comment']
labels = ["Imię", "Nazwisko", "Firma", "Telefon", "Email", "Komentarz"]


class AddressBookViewer(gi.repository.Gtk.Window):

    def __init__(self):
        super().__init__(title="Książka adresowa", window_position=gi.repository.Gtk.WindowPosition.CENTER)

        label = gi.repository.Gtk.Label(label="Utwórz lub otwórz książkę adresową wybierając opcję w menu Plik")

        self.main_layout = gi.repository.Gtk.Box(orientation=gi.repository.Gtk.Orientation.VERTICAL, homogeneous=False)
        self.main_layout.pack_start(self.__create_menu(False), False, False, 0)
        self.main_layout.pack_start(label, False, True, 12)

        self.add(self.main_layout)
        self.set_default_size(500, 400)

        self.connect("destroy", gi.repository.Gtk.main_quit)
        self.show_all()

    def __open_address_book(self, button):
        d = gi.repository.Gtk.FileChooserDialog(title="Wybierz plik książki adresowej",
                                                action=gi.repository.Gtk.FileChooserAction.OPEN)
        d.add_buttons(gi.repository.Gtk.STOCK_OK, gi.repository.Gtk.ResponseType.OK, gi.repository.Gtk.STOCK_CANCEL,
                      gi.repository.Gtk.ResponseType.CANCEL)
        if d.run() == gi.repository.Gtk.ResponseType.OK:
            try:
                w = ''.join(open(d.get_filename(), "r").readlines())
                people_list = json.loads(w)
                people_list.sort(key=lambda k: k['surname'])
                self.__create_table(people_list)
            except (Exception):
                m = gi.repository.Gtk.MessageDialog(title="Błąd",
                                                    text="Wystąpił błąd podczas odczytu z pliku. Upewnij się, że jest on dostępny i zawiera książkę adresową.",
                                                    message_type=gi.repository.Gtk.MessageType.ERROR, )
                m.add_buttons(gi.repository.Gtk.STOCK_OK, gi.repository.Gtk.ResponseType.OK)
                m.run()
                m.destroy()
        d.hide()

    def __save_address_book(self, button):
        d = gi.repository.Gtk.FileChooserDialog(title="Wybierz nazwę pliku książki adresowej",
                                                action=gi.repository.Gtk.FileChooserAction.SAVE)
        d.add_buttons(gi.repository.Gtk.STOCK_OK, gi.repository.Gtk.ResponseType.OK, gi.repository.Gtk.STOCK_CANCEL,
                      gi.repository.Gtk.ResponseType.CANCEL)
        if d.run() == gi.repository.Gtk.ResponseType.OK:
            try:
                with open(d.get_filename(), "w") as f:
                    f.write(json.dumps(self.__get_people_list()))

                m = gi.repository.Gtk.MessageDialog(title="Książka adresowa",
                                                    text="Poprawnie zapisano książkę adresową",
                                                    message_type=gi.repository.Gtk.MessageType.INFO)
                m.add_buttons(gi.repository.Gtk.STOCK_OK, gi.repository.Gtk.ResponseType.OK)
                m.run()
                m.destroy()
            except (Exception):
                m = gi.repository.Gtk.MessageDialog(title="Błąd",
                                                    text="Wystąpił błąd podczas zapisu do pliku. Upewnij się, że jest on dostępny i zawiera książkę adresową.",
                                                    message_type=gi.repository.Gtk.MessageType.ERROR, )
                m.add_buttons(gi.repository.Gtk.STOCK_OK, gi.repository.Gtk.ResponseType.OK)
                m.run()
                m.destroy()
        d.hide()

    def __create_address_book(self, button):
        self.__create_table([])

    def __create_menu(self, save):
        self.menu_bar = gi.repository.Gtk.MenuBar()

        file = gi.repository.Gtk.MenuItem(label="Plik")

        new_menuitem = gi.repository.Gtk.MenuItem(label="Nowy")
        new_menuitem.connect("activate", self.__create_address_book)

        open_menuitem = gi.repository.Gtk.MenuItem(label="Otwórz")
        open_menuitem.connect("activate", self.__open_address_book)

        self.save_menuitem = gi.repository.Gtk.MenuItem(label="Zapisz")
        self.save_menuitem.connect("activate", self.__save_address_book)
        if not save:
            self.save_menuitem.set_sensitive(False)

        file_menu = gi.repository.Gtk.Menu()
        file_menu.append(new_menuitem)
        file_menu.append(open_menuitem)
        file_menu.append(self.save_menuitem)

        file.set_submenu(file_menu)

        about = gi.repository.Gtk.MenuItem(label="O programie")

        author_menuitem = gi.repository.Gtk.MenuItem(label="Autor")
        author_menuitem.connect("activate", self.__about_author)

        about_menu = gi.repository.Gtk.Menu()
        about_menu.append(author_menuitem)

        about.set_submenu(about_menu)

        self.menu_bar.append(file)
        self.menu_bar.append(about)

        return self.menu_bar

    def __create_table(self, people_list):
        self.remove(self.main_layout)

        self.main_layout = gi.repository.Gtk.Box(orientation=gi.repository.Gtk.Orientation.VERTICAL, homogeneous=False)

        gid_layout = gi.repository.Gtk.Grid()
        gid_layout.set_column_homogeneous(True)
        gid_layout.set_row_homogeneous(True)

        self.main_layout.pack_start(self.__create_menu(True), False, False, 0)
        self.main_layout.pack_start(gid_layout, False, True, 0)
        self.add(self.main_layout)

        search_entry = gi.repository.Gtk.Entry()
        search_entry.connect("changed", self.__on_search_edited)

        __search_label = gi.repository.Gtk.Label(label="Szukaj")

        self.__people_liststore = gi.repository.Gtk.ListStore(str, str, str, str, str, str)
        for person in people_list:
            list = []
            for i in range(6):
                list.append(person[fields[i]])
            self.__people_liststore.append(list)
        self.__current_filter_name = ''

        self.__people_filter = self.__people_liststore.filter_new()
        self.__people_filter.set_visible_func(self.__name_filter_func)

        self.__treeview = gi.repository.Gtk.TreeView(model=self.__people_filter)
        on_edit_callbacks = [self.__cell_name_edited, self.__cell_surname_edited, self.__cell_company_edited,
                             self.__cell_phone_edited, self.__cell_email_edited, self.__cell_comment_edited]
        for i in range(6):
            renderer = gi.repository.Gtk.CellRendererText()
            renderer.set_property('editable', True)
            renderer.connect('edited', on_edit_callbacks[i], (self.__people_liststore, 1))
            column = gi.repository.Gtk.TreeViewColumn(labels[i], renderer, text=i)
            self.__treeview.append_column(column)

        add_button = gi.repository.Gtk.Button(label="Dodaj")
        add_button.connect("clicked", self.__on_add_button_clicked)
        delete_button = gi.repository.Gtk.Button(label="Usuń")
        delete_button.connect("clicked", self.__on_delete_button_clicked)

        scrollable_treelist = gi.repository.Gtk.ScrolledWindow()
        scrollable_treelist.set_vexpand(True)
        gid_layout.attach(__search_label, 2, 0, 1, 1)
        gid_layout.attach(search_entry, 3, 0, 1, 1)
        gid_layout.attach(scrollable_treelist, 0, 1, 4, 10)
        gid_layout.attach_next_to(add_button, scrollable_treelist, gi.repository.Gtk.PositionType.BOTTOM, 1,
                                  1)
        gid_layout.attach_next_to(delete_button, add_button, gi.repository.Gtk.PositionType.RIGHT, 1, 1)
        scrollable_treelist.add(self.__treeview)

        self.show_all()

    def __name_filter_func(self, model, iter, data):
        row_text = ''
        for i in range(6):
            row_text += ' ' + model[iter][i]
        return self.__current_filter_name.lower() in row_text.lower()

    def __on_search_edited(self, entry):
        self.__current_filter_name = entry.get_text()
        self.__people_filter.refilter()

    def __cell_name_edited(self, cell, row, value, listStore):
        self.__people_liststore[row][0] = value

    def __cell_surname_edited(self, cell, row, value, listStore):
        self.__people_liststore[row][1] = value

    def __cell_company_edited(self, cell, row, value, listStore):
        self.__people_liststore[row][2] = value

    def __cell_phone_edited(self, cell, row, value, listStore):
        self.__people_liststore[row][3] = value

    def __cell_email_edited(self, cell, row, value, listStore):
        self.__people_liststore[row][4] = value

    def __cell_comment_edited(self, cell, row, value, listStore):
        self.__people_liststore[row][5] = value

    def __on_add_button_clicked(self, widget):
        self.__people_liststore.append(("...", "", "", "", "", ""))

    def __on_delete_button_clicked(self, widget):
        selection = self.__treeview.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            current_iter = self.__people_liststore.get_iter_first()
            while current_iter:
                match = True
                for i in range(6):
                    if self.__people_liststore[current_iter][i] != model[treeiter][i]:
                        match = False
                        break
                if match:
                    self.__people_liststore.remove(current_iter)
                    return
                current_iter = self.__people_liststore.iter_next(current_iter)

    def __get_people_list(self):
        list = []
        current_iter = self.__people_liststore.get_iter_first()
        while current_iter:
            item = {}
            for column in range(6):
                item[fields[column]] = self.__people_liststore[current_iter][column]
            list.append(item)
            current_iter = self.__people_liststore.iter_next(current_iter)
        return list

    def __about_author(self, button):
        m = gi.repository.Gtk.MessageDialog(title="O programie",
                                            text="Autor: Marcel Korpal\nNumer indeksu: 175911\nOpis programu: książka adresowa pozwalająca zapisywać podstawowe informacje o osobach.",
                                            message_type=gi.repository.Gtk.MessageType.INFO)
        m.add_buttons(gi.repository.Gtk.STOCK_OK, gi.repository.Gtk.ResponseType.OK)
        m.run()
        m.destroy()


if __name__ == '__main__':
    try:
        w = AddressBookViewer()
        w.connect("delete-event", gi.repository.Gtk.main_quit)
        w.show_all()
        gi.repository.Gtk.main()
    except (IOError, UnicodeDecodeError):
        m = gi.repository.Gtk.MessageDialog(title="Błąd", text="Wystąpił błąd podczas uruchamiania aplikacji.",
                                            message_type=gi.repository.Gtk.MessageType.ERROR, )
        m.add_buttons(gi.repository.Gtk.STOCK_OK, gi.repository.Gtk.ResponseType.OK)
        m.run()
        m.destroy()
